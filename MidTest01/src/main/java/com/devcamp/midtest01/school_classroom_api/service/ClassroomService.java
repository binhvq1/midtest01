package com.devcamp.midtest01.school_classroom_api.service;

import java.util.ArrayList;

import org.springframework.stereotype.Service;

import com.devcamp.midtest01.school_classroom_api.models.Classroom;

@Service
public class ClassroomService {
    
    Classroom class1 = new Classroom(01, "A01", 10);
    Classroom class2 = new Classroom(02, "A02", 11);
    Classroom class3 = new Classroom(03, "A03", 12);

    Classroom class4 = new Classroom(04, "A04", 20);
    Classroom class5 = new Classroom(05, "A05", 21);
    Classroom class6 = new Classroom(06, "A06", 22);
    
    Classroom class7 = new Classroom(107, "A07", 30);
    Classroom class8 = new Classroom(108, "A08", 31);
    Classroom class9 = new Classroom(109, "A09", 32);

    public ArrayList<Classroom> getClassRoom1(){
        ArrayList<Classroom> classRoomList1 = new ArrayList<>();
        classRoomList1.add(class1);
        classRoomList1.add(class2);
        classRoomList1.add(class3);
        return classRoomList1;
    }
    public ArrayList<Classroom> getClassRoom2(){
        ArrayList<Classroom> classRoomList2 = new ArrayList<>();
        classRoomList2.add(class4);
        classRoomList2.add(class5);
        classRoomList2.add(class6);
        return classRoomList2;
    }
    public ArrayList<Classroom> getClassRoom3(){
        ArrayList<Classroom> classRoomList3 = new ArrayList<>();
        classRoomList3.add(class7);
        classRoomList3.add(class8);
        classRoomList3.add(class9);
        return classRoomList3;
    }
    public ArrayList<Classroom> getAllClassRoom(){
        ArrayList<Classroom> classRoomList = new ArrayList<>();
        classRoomList.add(class1);
        classRoomList.add(class2);
        classRoomList.add(class3);
        classRoomList.add(class4);
        classRoomList.add(class5);
        classRoomList.add(class6);
        classRoomList.add(class7);
        classRoomList.add(class8);
        classRoomList.add(class9);
        return classRoomList;
    }
}
