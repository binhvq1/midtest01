package com.devcamp.midtest01.school_classroom_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest01.school_classroom_api.models.Classroom;
import com.devcamp.midtest01.school_classroom_api.service.ClassroomService;

@RestController
@CrossOrigin
public class ClassroomController {
    @Autowired
    private ClassroomService classRoomService;

    @GetMapping("classrooms")
    public ArrayList<Classroom> getAllClass(){
        ArrayList<Classroom> listClass = classRoomService.getAllClassRoom();
        return listClass;
    }

    @GetMapping("classroom-info")
    public ArrayList<Classroom> getClassfromStudentqty(@RequestParam(required = true, name = "qty")int qty){
        ArrayList<Classroom> listClass = classRoomService.getAllClassRoom();

        ArrayList<Classroom> findClass = new ArrayList<Classroom>();
        for(Classroom classroom : listClass){
            if(classroom.getNoStudent() >= qty)
            findClass.add(classroom);
        }
        return findClass;
    }
}
