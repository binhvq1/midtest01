package com.devcamp.midtest01.school_classroom_api.controller;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.midtest01.school_classroom_api.models.School;
import com.devcamp.midtest01.school_classroom_api.service.SchoolService;

@RestController
@CrossOrigin
public class SchoolController {
    @Autowired
    private SchoolService schoolService;

    @GetMapping("schools")
    public ArrayList<School> getAllSchool(){
        ArrayList<School> allSchool = schoolService.getAllSchool();
        return allSchool;
    }

    @GetMapping("school-info")
    public School getSchoolInfo(@RequestParam(required = true, name="schoolId") int schoolId) {
        ArrayList<School> allSchool = schoolService.getAllSchool();

        School findSchool = new School();
        for (School school : allSchool) {
            if(school.getId() == schoolId) {
                findSchool = school ;
            }
        }
        return findSchool;
    }

    @GetMapping("shool-find")
    public ArrayList<School> getSchoolfromStudentqty(@RequestParam(required = true, name = "qty")int qty){
        ArrayList<School> allSchool = schoolService.getAllSchool();
        
        ArrayList<School> findSchoolByQty = new ArrayList<School>();
        
        for(School school : allSchool){
            int sum = 0;
            sum = school.getTotalStudent();
            if(sum >= qty)
            findSchoolByQty.add(school);
        }
        return findSchoolByQty;
    }

}
