package com.devcamp.midtest01.school_classroom_api.service;

import java.util.ArrayList;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.midtest01.school_classroom_api.models.School;

@Service
public class SchoolService {
    @Autowired
    private ClassroomService classRoomService;

    public ArrayList<School> getAllSchool(){
        School schoolA = new School(001, "Chu Van An", "Ha Noi", classRoomService.getClassRoom1());
        School schoolB = new School(010, "Luong The Vinh", "Binh Duong", classRoomService.getClassRoom2());
        School schoolC = new School(033, "Nguyen Khuyen", "TP HCM", classRoomService.getClassRoom3());

    
        System.out.println(schoolA.getTotalStudent());
        ArrayList<School> allSchool = new ArrayList<>();
        allSchool.add(schoolA);
        allSchool.add(schoolB);
        allSchool.add(schoolC);
        return allSchool;
    }
}
